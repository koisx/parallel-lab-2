import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private final static Integer SERVER_PORT = 1234;
    private static boolean doesUserWantToLogOut = false;

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        InetAddress ip = InetAddress.getByName("localhost");
        Socket socket = new Socket(ip, SERVER_PORT);

        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

        System.out.println("Enter username");
        String username = scanner.nextLine();
        dataOutputStream.writeUTF(username);

        new Thread(() ->
        {
            while (true) {
                if (doesUserWantToLogOut) break;
                String message = scanner.nextLine();

                try {
                    dataOutputStream.writeUTF(message);
                    if (message.equals("exit")) doesUserWantToLogOut = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() ->
        {
            while (true) {
                if (doesUserWantToLogOut) break;
                try {
                    String message = dataInputStream.readUTF();
                    System.out.println(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }
}
