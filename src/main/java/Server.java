import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.StringTokenizer;

public class Server {
    private static HashMap<String, DataOutputStream> connections = new HashMap<>();
    private static final int PORT_NUMBER = 1234;

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT_NUMBER);
            Socket socket;

            while (true) {
                socket = serverSocket.accept();
                DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                String name = letClientIn(dataInputStream, dataOutputStream);
                serveClient(dataInputStream, dataOutputStream, name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Let client in add register them in c client connection pool.
     * @param dataInputStream input data
     * @param dataOutputStream output data
     * @return refistered client name
     * @throws IOException IO exception
     */
    private static String letClientIn(DataInputStream dataInputStream, DataOutputStream dataOutputStream) throws IOException {

        String possibleName = dataInputStream.readUTF();
        while (connections.containsKey(possibleName)) {
            dataOutputStream.writeUTF("Username already exists, enter another one.");
            possibleName = dataInputStream.readUTF();
        }
        String name = possibleName;
        connections.put(name, dataOutputStream);
        System.out.println("<<<" + name + " has entered chat room>>>");
        broadcastMessage("<<<" + name + " has entered chat room>>>");
        dataOutputStream.writeUTF("Enter 'exit' to logout.");
        return name;
    }

    /**
     * Brodcasting for a single chat room participant identified by name param
     * or for an participant except message sender.
     * @param message message to be broadcasted
     * @param name nme of the message recipient
     */
    private static void broadcastMessage(String message, String name) {
        if (message.contains("#")) {
            StringTokenizer stringTokenizer = new StringTokenizer(message, "#");
            String newMessage = stringTokenizer.nextToken();
            String recipient = stringTokenizer.nextToken();
            connections.forEach((k, v) ->
            {
                if (k.equals(recipient)) {
                    try {
                        v.writeUTF("<" + name + "> : " + newMessage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            connections.forEach((k, v) ->
            {
                if (!k.equals(name)) {
                    try {
                        v.writeUTF(name + " : " + message);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * Broadcasting for each participant of the chat room (including the message sender)
     * @param message message to be broadcasted
     */
    private static void broadcastMessage(String message) {
        connections.forEach((k, v) ->
        {
            try {
                v.writeUTF(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Serve individual client request
     * @param dataInputStream input data
     * @param dataOutputStream output data
     * @param name client name
     */
    private static void serveClient(DataInputStream dataInputStream, DataOutputStream dataOutputStream, String name) {
        new Thread(() ->
        {
            try {
                while (true) {
                    String message = dataInputStream.readUTF();
                    System.out.println(name + " : " + message);
                    if (message.equals("exit")) {
                        connections.remove(name);
                        dataOutputStream.writeUTF("You have disconnected");
                        dataInputStream.close();
                        dataOutputStream.close();
                        System.out.println("<<<" + name + " has left chat room>>>");
                        broadcastMessage("<<<" + name + " has left chat room>>>");
                        break;
                    }
                    broadcastMessage(message, name);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
